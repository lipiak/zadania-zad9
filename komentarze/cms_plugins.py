from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _
from forms import KomentarzForm
from models import Komentarz, KomentarzPlugin
from django.utils import timezone

class KomentarzePlugin(CMSPluginBase):
    model = CMSPlugin
    name = _("Komentarze plugin")
    render_template = "komentarze/komentarze_wtyczka.html"
    cache = False

    def render(self, context, instance, placeholder):
        request = context['request']
        print 'weszlo1'
        print request.method
        if request.method == 'POST':
                form = KomentarzForm(request.POST)
                print 'weszlo2'
                if form.is_valid():
                    user = form.cleaned_data['user']
                    tresc = form.cleaned_data['tresc']
                    strona = request.current_page
                    data = timezone.datetime.now()
                    k = Komentarz(autor=user, data=data, tresc=tresc, strona=strona)
                    k.save()

        context.update({
                'instance': instance,
                'placeholder': placeholder,
                'komentarze': Komentarz.objects.all().filter(strona=request.current_page).order_by('-data'),
                'forma': KomentarzForm()
        })

        return context

plugin_pool.register_plugin(KomentarzePlugin)