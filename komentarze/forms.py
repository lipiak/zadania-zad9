# -*- coding: utf-8 -*
from django import forms
from models import Komentarz
from django.forms import ModelForm, HiddenInput

class KomentarzForm(forms.Form):
    user = forms.CharField()
    tresc = forms.CharField(widget=forms.Textarea())


