from django.db import models
from cms.models.pluginmodel import CMSPlugin

class Komentarz(models.Model):
    autor = models.CharField('autor_komentarza', max_length=100)
    data = models.DateTimeField('data komentarza')
    tresc = models.CharField('tresc_wpisu',max_length=1000)
    strona = models.URLField('strona kometarza')

    def __unicode__(self):
        return unicode.format(u'{0} {1} {2} {3}', self.autor, self.data, self.tresc, self.strona)

class KomentarzPlugin(CMSPlugin):
    szerokosc = models.IntegerField(help_text="Szerokosc pola dodawania komentarzy")
    wysokosc = models.IntegerField(help_text="Wysokosc pola dodawania komentarzy")

    def __unicode__(self):
        return u'test'