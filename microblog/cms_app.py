from django.utils.translation import ugettext_lazy as _

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

class MicroblogApp(CMSApp):
    name = _("Microblog App")        # give your app a name, this is required
    urls = ["microblog.urls"]       # link your app to url configuration(s)

apphook_pool.register(MicroblogApp) # register your app