# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

class BlogMenu(CMSAttachMenu):
    name = _("Blog menu")
    cache = False

    def get_nodes(self, request):
        nodes = []

        if not request.user.is_authenticated():
            n1 = NavigationNode('Zaloguj', reverse('bloglogin'), 1, attr={'visible_for_anonymous': True})
            nodes.append(n1)

        n2 = NavigationNode('Wyloguj', reverse('bloglogout'), 2, attr={'visible_for_anonymous': False})
        nodes.append(n2)

        if not request.user.is_authenticated():
            n3 = NavigationNode('Zarejestruj', reverse('register'), 3, attr={'visible_for_anonymous': True})
            nodes.append(n3)

        n4 = NavigationNode('Dodaj wpis', reverse('microblogDodaj'), 4, attr={'visible_for_anonymous': False})
        nodes.append(n4)

        n5 = NavigationNode('Filtruj po użytkownikach', reverse('filter'), 5, attr={'visible_for_anonymous': False})
        nodes.append(n5)

        n6 = NavigationNode('Filtruj po miesiącach', reverse('filterDate'), 6, attr={'visible_for_anonymous': False})
        nodes.append(n6)

        return nodes

menu_pool.register_menu(BlogMenu)
