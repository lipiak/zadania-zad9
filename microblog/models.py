from django.db import models

from django.utils import timezone

class Wpis(models.Model):
    autor = models.CharField(max_length=100)
    tresc = models.CharField('tresc wpisu', max_length=500)
    data = models.DateTimeField('data wpisu')

    def __unicode__(self):
        return unicode.format(u'{0} {1}: {2}', self.data, self.autor, self.tresc)

class User(models.Model):
    login = models.CharField(max_length=100)
    haslo = models.CharField(max_length=100)

    def __unicode__(self):
        return unicode.format(u'{0} {1}', self.login, self.haslo)


