from django.conf.urls import patterns, url
from views import *

from django.contrib import admin

admin.autodiscover()


urlpatterns = patterns('',
        url(r'^$', index, name='microblogIndex'),
        url(r'^dodaj/', dodaj, name='microblogDodaj'),
        url(r'^add/', add, name='add'),
        url(r'^login/', bloglogin, name='bloglogin'),
        url(r'^doLogin/', doLogin, name='wykonajLogowanie'),
        url(r'^logout/', bloglogout, name='bloglogout'),
        url(r'^register/', register, name='register'),
        url(r'^doRegister/', doRegister, name='doRegister'),
        url(r'^komunikat/(?P<typ>\w+)/(?P<message>\w+)/$', komunikat, name='komunikat'),
        url(r'^filter/', filter, name='filter'),
        url(r'^doFilter/', doFilter, name='doFilter'),
        url(r'^doFilterDate/', doFilterDate, name='doFilterDate'),
        url(r'^showuserposts/(?P<user>\w+)/', showUserPosts, name='showUserPosts'),
        url(r'^showdateposts/(?P<date>\w+)/', showDatePosts, name='showDatePosts'),
        url(r'^filterDate/', filterDate, name='filterDate'),
)