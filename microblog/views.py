# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils import timezone

from django import forms

from django.shortcuts import render
from django.http import HttpResponseRedirect

from models import Wpis

from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate


class DodajForm(forms.Form):
    wpis = forms.CharField(max_length=500)

class LoginForm(forms.Form):
    login = forms.CharField(max_length=100)
    haslo = forms.CharField(max_length=32, widget=forms.PasswordInput)

class RegisterForm(forms.Form):
    login = forms.CharField(max_length=100)
    email = forms.EmailField()
    haslo = forms.CharField(max_length=32, widget=forms.PasswordInput)
    nhaslo = forms.CharField(max_length=32, widget=forms.PasswordInput)

class FilterForm(forms.Form):
    nazwa_uzytkownika = forms.CharField(max_length=100)

class FilterDateForm(forms.Form):
    CHOICES = ([
        ('1', 'Styczeń'),
        ('2', 'Luty'),
        ('3', 'Marzec'),
        ('4', 'Kwiecień'),
        ('5', 'Maj'),
        ('6', 'Czerwiec'),
        ('7', 'Lipiec'),
        ('8', 'Sierpień'),
        ('9', 'Wrzesień'),
        ('10', 'Październik'),
        ('11', 'Listopad'),
        ('12', 'Grudzień'),
    ])
    miesiac = forms.ChoiceField(choices=CHOICES)

def dodaj(request):
    form = DodajForm() # An unbound form
    return render(request, 'microblog/dodawanie.html', {
        'forma': form,
        'zalogowany': request.user.is_authenticated()})

def bloglogin(request):
    forma = LoginForm()
    return render(request, 'microblog/login.html', {
        'forma': forma,
        'zalogowany': request.user.is_authenticated()})

def register(request):
    forma = RegisterForm()
    return render(request, 'microblog/register.html', {
        'forma': forma,
        'zalogowany': request.user.is_authenticated()})

def doLogin(request):
    forma = LoginForm(request.POST)

    if request.method == 'POST':

        if forma.is_valid():
            username = forma.cleaned_data['login']
            haslo = forma.cleaned_data['haslo']
            user = authenticate(username=username, password=haslo)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    #request.session['user'] = username
                    return HttpResponseRedirect(reverse('komunikat', args=('ok', 'login_ok',)))
            else:
                #request.session['user'] = None
                return HttpResponseRedirect(reverse('komunikat', args=('cokolwiek', 'cokolwiek',)))
        else:
            return HttpResponseRedirect(reverse('komunikat', args=('cokolwiek', 'cokolwiek',)))

    else:
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('komunikat', args=('cokolwiek', 'cokolwiek',)))
        else:
            return HttpResponseRedirect(reverse('bloglogin'))

def doRegister(request):
    forma = RegisterForm(request.POST)

    if request.method == 'POST':

        if forma.is_valid():
            username = forma.cleaned_data['login']
            email = forma.cleaned_data['email']
            haslo = forma.cleaned_data['haslo']
            nhaslo = forma.cleaned_data['nhaslo']
            ilosc = User.objects.filter(login=username).count()
            if ilosc >= 1:
                return HttpResponseRedirect(reverse('komunikat', args=('blad', 'user_exists',)))
            else:
                if not haslo == nhaslo:
                    return HttpResponseRedirect(reverse('komunikat', args=('blad', 'pwd_not_match',)))


                u = User.objects.create_user(username, email, haslo)
                u.save()
                return HttpResponseRedirect(reverse('komunikat', args=('ok', 'register_ok',)))

    else:
        return HttpResponseRedirect(reverse('register'))

def komunikat(request, typ, message):
    tresc = ''
    if message == 'login_ok':
        tresc = 'Zalogowano pomyslnie'
    elif message == 'logout_ok':
        tresc = 'Wylogowano pomyslnie'
    elif message == 'not_logged_in':
        tresc = 'Nie jestes zalogowany'
    elif message == 'wpis_ok':
        tresc = 'Twoj wpis dodano pomyslnie'
    elif message == 'user_exists':
        tresc = 'Taki uzytkownik juz istnieje'
    elif message == 'pwd_not_match':
        tresc = 'Podane hasla nie sa identyczne'
    elif message == 'register_ok':
        tresc = 'Rejestracja poprawna. Mozesz sie zalogowac'
    else:
        tresc = 'Cos chyba kombinujesz'

    if typ == 'ok' or typ == 'blad':
        return render(request, 'microblog/komunikat.html',
            {
                'typ': typ,
                'tresc': tresc,
                'zalogowany': request.user.is_authenticated(),
            })
    else:
        return render(request, 'microblog/komunikat.html',
            {
                'typ': 'blad',
                'tresc': tresc,
                'zalogowany': request.user.is_authenticated(),
            })



def index(request):

    return render(request, 'microblog/index.html', {
        'zalogowany': request.user.is_authenticated(),
        'wszystkie_wpisy': Wpis.objects.all().order_by('-data'),
    })

def bloglogout(request):
    if request.user.is_authenticated():
        logout(request)
        return HttpResponseRedirect(reverse('komunikat', args=('ok', 'logout_ok',)))
    else:
        return HttpResponseRedirect(reverse('komunikat', args=('blad', 'not_logged_in',)))



def add(request):
    if request.user.is_authenticated():
        if request.method == 'POST': # If the form has been submitted...
            # ContactForm was defined in the the previous section
            form = DodajForm(request.POST) # A form bound to the POST data
            if form.is_valid():
                user = request.user
                tresc = form.cleaned_data['wpis']
                data = timezone.now()
                w = Wpis(autor=user.username, tresc=tresc, data=data)
                w.save()
                return HttpResponseRedirect(reverse('komunikat', args=('ok', 'wpis_ok',)))

        return HttpResponseRedirect(reverse('komunikat', args=('blad', 'fdsfds',)))

    else:
        return HttpResponseRedirect(reverse('komunikat', args=('blad', 'not_logged_in',)))

def filter(request):
    forma = FilterForm()
    return render(request, 'microblog/filter.html', {
        'forma': forma,
        'zalogowany': request.user.is_authenticated()})

def doFilter(request):

    if request.method == 'POST':
        forma = FilterForm(request.POST)

        if forma.is_valid():
            user = forma.cleaned_data['nazwa_uzytkownika']
            return HttpResponseRedirect(reverse('showUserPosts', args=(user,)))

    return HttpResponseRedirect(reverse('microblogIndex'))

def showUserPosts(request, user):
    return render(request, 'microblog/index.html', {
        'zalogowany': request.user.is_authenticated(),
        'wszystkie_wpisy': Wpis.objects.filter(autor=user).order_by('-data'),
        })

def showDatePosts(request, date):
    return render(request, 'microblog/index.html', {
        'zalogowany': request.user.is_authenticated(),
        'wszystkie_wpisy': Wpis.objects.filter(data__month=int(date)).order_by('-data'),
        })

def filterDate(request):
    forma = FilterDateForm()
    return render(request, 'microblog/filtermonth.html', {
        'forma': forma,
        'zalogowany': request.user.is_authenticated()})

def doFilterDate(request):

    if request.method == 'POST':
        form = FilterDateForm(request.POST)

        if form.is_valid():
            miesiac = form.cleaned_data['miesiac']
            return HttpResponseRedirect(reverse('showDatePosts', args=(miesiac,)))

    return HttpResponseRedirect(reverse('microblogIndex'))


